## 功能说明

- 支持SWD接口
- 支持虚拟串口
- 支持hid或winusb通讯，win10无需安装驱动；win7winusb安装驱动

## 使用说明

### 如何连接立创·梁山派

下载器与立创梁山派之间通过sh1.0 1x6pin反排线连接，下载器通过TypeC数据线连接电脑为立创·梁山派提供代码下载与串口调试；

![20221209-164337](image/20221209-164337.jpg)

![20221209-164350](image/20221209-164350.jpg)

打开mdk，点击魔术棒，设置调试工具为CMSIS-DAP，点击Settings

![image-20221209170223865](image/image-20221209170223865.png)

CMSIS-DAP v1与lckfb.com-v1.1.0H表示HID版本，Port这里需要选择SWJ模式否则无法识别，IDCODE需要正确连接到开发板后才能找到

![image-20221209170724551](image/image-20221209170724551.png)

CMSIS-DAP v2与lckfb.com-v1.1.0W表示WINUSB版本

![image-20221209171541167](image/image-20221209171541167.png)

正常情况下DAPlink是能被识别到，如果未识别到CMSIS-DAP请鼠标选题开始按钮右键打开设备管理器

![image-20221209164941437](image/image-20221209164941437.png)

HID版本正常情况下会在设备管理器中发现以下两个驱动，如果出现感叹号说明有问题；

![image-20221209165103710](image/image-20221209165103710.png)

winusb版本正常情况下会在设备管理器中发现以下两个驱动，如果出现感叹号说明有问题；

![image-20221209171505301](image/image-20221209171505301.png)

## 文件说明

DAPLINK
│  CH552.H		//标准头文件
│  DAP.c			 //DAP源码
│  DAP.h			 //DAP头文件
│  Debug.C		 //芯片配置
│  Debug.H		 //芯片配置头文件
│  Main_Usb.C	//主程序与USB中断服务函数
│  readme.md
│  SW_DP.c		 //SWD源码
│  Uart.c		      //串口
│  Uart.h		      //串口头文件
│  Uart1Dbg.C	//调试串口
│  Uart1Dbg.H	//调试串口
├─firmware		//固件
│      LCKFB_DAPLink_HID_20221125.hex	
├─image	         //图片
├─KEIL	
│  │  CH55x_DAPLink.uvproj	//keil4工程文件
│ 
│
└─tool
        CMSIS_DAP.dll		//5.28winusb无法识别补丁
        keil4.exe			     //keil4安装包
        WCHISPTool_Setup.exe//下载工具

## 软件说明

- 通过宏定义DAP_USE_HID_FW_V1使用hid协议或winusb协议，winusb在win7中无法自动安装驱动且mdk版本小于5.28中无法识别

  ![image-20221209161740998](image/image-20221209161740998.png)

  - winusb在mdk小于5.28版本无法识别解决方法；
    - 切换为HID；
    - mdk升级到大于5.28以后的版本；
    - 添加CMSIS_DAP.dll(放tool目录下)补丁到C:\Keil_v5\ARM\BIN


  

## 硬件说明

- 开源链接：https://oshwhub.com/lengyuefeng/a7c57e6d86bd47789178df3fda9219dc
- 主控：CH552G
- 通过SH1.0 1x6PIN接口反线序与立创·梁山派连接
- 注意：UART&SWD IO均为5V

## 环境搭建

### 安装keil4（安装包在tool目录下）

参考其他安装教程

### 安装烧入工具（安装包在tool目录下）

#### 双击运行

![img](image/16705775471968.png)

#### 下一步

![img](image/16705775471943.png)

![img](image/16705775471944.png)

![img](image/16705775471955.png)

![img](image/16705775471956.png)

![img](image/16705775471957.png)

#### 添加库到keil4

![image-20221209172401156](image/image-20221209172401156.png)

添加成功

![image-20221209172531915](image/image-20221209172531915.png)

重新打开keil4，点击魔术棒已经可以选WCH相关芯片了

![image-20221209172647311](image/image-20221209172647311.png)

## 固件烧入

![image-20221209173323393](image/image-20221209173323393.png)

### 插入设备

#### 首次直接进入升级模式

没有烧入过固件的设备，第一次插入会默认进入到升级模式

![img](image/167057813901322.jpeg)

#### 手动进入升级模式（第一次插入可以跳过这个步骤不看）

如果之前烧入过固件的需要短接进入到升级模式

拔下USB线给设备断电，然后短接靠USB这边的最上面引脚和第五个引脚不用放开，在短接的时候插入到电脑USB，usb插入以后1s中后松开短接，这个时候ch552会进入到升级模式，下面软件会识别到设备插入

![img](image/167057813901323.png)

![img](image/167057813901324.jpeg)

#### 软件识别

正常情况下插入以后1为自动搜索到设备，2并显示设备UID

![image-20221209173412911](image/image-20221209173412911.png)

#### 下载

点击下载，完成后显示失败：0表示成功

![image-20221209173433646](image/image-20221209173433646.png)

## 鸣谢

https://whycan.com/t_3732.html

https://whycan.com/t_7786.html

https://github.com/wuxx/nanoDAP

